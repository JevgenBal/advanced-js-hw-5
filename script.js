const getUsers = () => {
  return fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(data => {
      const userArray = data.map(user => {
        return {
          id: user.id,
          firstName: user.name.split(' ')[0],
          lastName: user.name.split(' ')[1],
          email: user.email
        };
      });
      return userArray;
    });
};

const getPosts = () => {
  return fetch('https://ajax.test-danit.com/api/json/posts')
    .then(response => response.json())
    .then(data => data);
};

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.cardElement = null;
  }

  createCardElement() {
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');

    const headerElement = document.createElement('h3');
    headerElement.textContent = this.post.title;

    const textElement = document.createElement('p');
    textElement.textContent = this.post.body;

    const userElement = document.createElement('p');
    userElement.textContent = `${this.user.firstName} ${this.user.lastName} (${this.user.email})`;

    const deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';

    const deleteIcon = document.createElement('i');
    deleteIcon.classList.add('fa-brands', 'fa-twitter');

    deleteButton.appendChild(deleteIcon);
    deleteButton.addEventListener('click', () => this.deleteCard());

    cardElement.appendChild(headerElement);
    cardElement.appendChild(textElement);
    cardElement.appendChild(userElement);
    cardElement.appendChild(deleteButton);

    this.cardElement = cardElement; 

    return cardElement;
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          this.cardElement.remove();
        } else {
          throw new Error('Failed to delete the card.');
        }
      })
      .catch(error => console.error(error));
  }
}

const displayCards = async () => {
  try {
    const users = await getUsers();
    const posts = await getPosts();

    const cardsContainer = document.getElementById('cards-container');

    posts.forEach(post => {
      const user = users.find(user => user.id === post.userId);
      const card = new Card(post, user);
      const cardElement = card.createCardElement();
      cardsContainer.appendChild(cardElement);
    });
  } catch (error) {
    console.error(error);
  }
};

displayCards();
